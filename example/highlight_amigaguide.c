#define __NOLIBBASE__
#include <proto/exec.h>

#include <proto/alib.h>
#include <proto/locale.h>

#include <tools/textedit/filetypes.h>

extern struct Library *SysBase;
extern struct Locale *Locale;
extern struct Catalog       *Catalog;


void ensureOpenLibs(void);

static void __ASM__ init(__REG__(a0, APTR userData));

void __ASM__ terminate(__REG__ (a0, struct Filetype *fType));
Object * __ASM__ settingsGadget(__REG__(a0, struct Filetype *fType), __REG__(d0, ULONG i));
STRPTR __ASM__ settingsTitle(__REG__(a0, struct Filetype *fType), __REG__(d0, ULONG i));
void __ASM__ applySettings(__REG__(a0, struct Filetype *fType));
void __ASM__ setSettingsFromGUI(__REG__(a0, struct Filetype *fType));
void __ASM__ setGUIFromSettings(__REG__(a0, struct Filetype *fType), __REG__(a1, struct Window *window));
void __ASM__ disposeGadgets(__REG__(a0, struct Filetype *fType));
void __ASM__ processGadgetUp(__REG__(a0, struct Filetype *fType), __REG__(d0, UWORD gadgetId), __REG__(a1, struct Window *window));
BOOL __ASM__ saveHighlightSettings(__REG__(a0, struct IFFHandle *iff), __REG__(a1, struct ContextNode *cn), __REG__(a2, APTR data));
BOOL __ASM__ loadHighlightSettings(__REG__(a0, struct IFFHandle *iff), __REG__(a1, struct ContextNode *cn), __REG__(a2, APTR data));

#define INT_VERSION    1
#define INT_REVISION   0

struct Plugin filetypePlugin =
{
  NULL,
  FILETYPEPLUGIN_VERSION,
  MASTER_VERSION,
  INT_VERSION,
  INT_REVISION,
  0,
  0,
  &init
};

#define HL_PLAIN 0
#define HL_KEYWORD 1
#define HL_KEYWORDSTR 2
#define HL_KEYWORDARG 3
#define HL_NODEKEYWORD 4

static UWORD defaultStyleCodes[5] =
{
    (0<<8)|0, // plain
    (1<<8)|TBSTYLE_SETCOLOR|TBSTYLE_BOLD, // keyword
    (2<<8)|TBSTYLE_SETCOLOR, // keyword string
    (2<<8)|TBSTYLE_SETCOLOR|TBSTYLE_BOLD, // keyword arg
    (1<<8)|TBSTYLE_SETCOLOR|TBSTYLE_BOLD // node and endnode
};

static CONST_STRPTR stylename[5];


static char *globalcommands[] = {
   "@$VER:"
 , "@(C)"
 , "@AUTHOR"
 , "@DATABASE"
 , "@DNOE"
 , "@FONT"
 , "@HEIGHT"
 , "@HELP"
 , "@INDEX"
 , "@MACRO"
 , "@MASTER"
 , "@ONCLOSE"
 , "@ONOPEN"
 , "@REMARK"
 , "@REM"
 , "@SMARTWRAP"
 , "@TAB"
 , "@WIDTH"
 , "@WORDWRAP"
 , "@KEYWORDS"
 , "@NEXT"
 , "@PREV"
 , "@TITLE"
 , "@TOC"
 , NULL
};


static char *nodecommands[] = {
   "@NODE"
 , "@ENDNODE"
 , NULL
};

ULONG compositAddStr(struct Filetype *fType, struct Hook *hook, ULONG startState)
{
	ULONG stringstartstate;
	ULONG stringwildstate;
	ULONG stringendstate;

	stringstartstate = fType->createHighlightState(hook, TEHL_STATE_APPLYFORMAT, '"', HL_KEYWORDSTR);
	fType->linkHighlightState(hook, startState, stringstartstate);

	stringwildstate = fType->createHighlightState(hook, TEHL_STATE_APPLYFORMAT|TEHL_STATE_SELFREPEATING, CHAR_ALWAYS, HL_KEYWORDSTR);
	fType->linkHighlightState(hook, stringstartstate, stringwildstate);
	
	stringendstate = fType->createHighlightState(hook, TEHL_STATE_APPLYFORMAT, '"', HL_KEYWORDSTR);
	fType->linkHighlightState(hook, stringwildstate, stringendstate);

	return stringendstate;
}

void setupAttribCommands(struct Filetype *fType, struct Hook *hook, ULONG startState)
{
	ULONG curlystate;
	ULONG cmdstringendstate;
	ULONG cmdwildstate;
	ULONG whitespacestate;
	ULONG precmdwhitespacestate;
	ULONG argstringendstate;
	ULONG argwildstate;
	ULONG endstate;
	ULONG afterendstate;

	curlystate = fType->createHighlightState(hook, TEHL_STATE_APPLYFORMAT, '{', HL_KEYWORD);
	fType->linkHighlightState(hook, startState, curlystate);

	cmdstringendstate = compositAddStr(fType, hook, curlystate);

	cmdwildstate = fType->createHighlightState(hook, TEHL_STATE_APPLYFORMAT|TEHL_STATE_SELFREPEATING, CHAR_ALWAYS, HL_KEYWORD);
	fType->linkHighlightState(hook, curlystate, cmdwildstate);
	
	// After argstringendstate comes a cmdwildstate (but we need some whitespace inbetween)
	precmdwhitespacestate = fType->createHighlightState(hook, TEHL_STATE_SELFREPEATING, CHAR_SPACE, 0);
	fType->linkHighlightState(hook, cmdstringendstate, precmdwhitespacestate);
	fType->linkHighlightState(hook, precmdwhitespacestate, cmdwildstate);

	// After cmdwildstate comes a series of arguments (either string or other)
	whitespacestate = fType->createHighlightState(hook, TEHL_STATE_SELFREPEATING, CHAR_SPACE, 0);
	fType->linkHighlightState(hook, cmdwildstate, whitespacestate);

	argstringendstate = compositAddStr(fType, hook, whitespacestate);
	fType->linkHighlightState(hook, argstringendstate, whitespacestate);

	argwildstate = fType->createHighlightState(hook, TEHL_STATE_APPLYFORMAT|TEHL_STATE_SELFREPEATING, CHAR_ALWAYS, HL_KEYWORDARG);
	fType->linkHighlightState(hook, whitespacestate, argwildstate);
	fType->linkHighlightState(hook, argwildstate, whitespacestate);

	// We can pretty much end the command from anywhere
	endstate = fType->createHighlightState(hook, TEHL_STATE_APPLYFORMAT, '}', HL_KEYWORD);
	fType->linkHighlightState(hook, cmdwildstate, endstate);
	fType->linkHighlightState(hook, curlystate, endstate);
	fType->linkHighlightState(hook, cmdstringendstate, endstate);
	fType->linkHighlightState(hook, argstringendstate, endstate);
	fType->linkHighlightState(hook, argwildstate, endstate);
	fType->linkHighlightState(hook, whitespacestate, endstate);

	afterendstate = fType->createHighlightState(hook, TEHL_STATE_APPLYFORMAT|TEHL_STATE_AUTOINITIALSTATE, CHAR_ALWAYS, HL_KEYWORD);
	fType->linkHighlightState(hook, endstate, startState);
	fType->linkHighlightState(hook, endstate, afterendstate);
}

ULONG setupGlobalAttribs(struct Filetype *fType, struct Hook *hook)
{
	ULONG whitespacestate;
	ULONG argstringendstate;
	ULONG argwildstate;

	// After cmd we need to go into a wildstate follwoed by
	// a series of arguments (either string or other)
	whitespacestate = fType->createHighlightState(hook, TEHL_STATE_SELFREPEATING, CHAR_SPACE, 0);

	argstringendstate = compositAddStr(fType, hook, whitespacestate);
	fType->linkHighlightState(hook, argstringendstate, whitespacestate);
	fType->linkHighlightState(hook, whitespacestate, argstringendstate);

	argwildstate = fType->createHighlightState(hook, TEHL_STATE_APPLYFORMAT|TEHL_STATE_SELFREPEATING, CHAR_ALWAYS, HL_KEYWORDARG);
	fType->linkHighlightState(hook, whitespacestate, argwildstate);
	fType->linkHighlightState(hook, argwildstate, whitespacestate);

	return whitespacestate;
}

static void __ASM__ init(__REG__(a0, APTR userData))
{
	struct Hook *hook;
    struct Filetype *fType = (struct Filetype *)userData;
	ULONG cmdstartstate;
	ULONG globalAttribState;
	ULONG tmp;
	struct HighlighterGUI *hlGUI;

    int i;

    ensureOpenLibs();

	fType->typeName = "AmigaGuide";
	fType->autoFileTypes = "amigaguide";
	fType->terminate = terminate;
	fType->settingsTitle = settingsTitle;
	fType->settingsGadget = settingsGadget;
	fType->processGadgetUp = processGadgetUp;
	fType->setSettingsFromGUI = setSettingsFromGUI;
	fType->setGUIFromSettings = setGUIFromSettings;
	fType->applySettings = applySettings;
	fType->disposeGadgets = disposeGadgets;
	fType->saveSettings = saveHighlightSettings;
	fType->loadSettings = loadHighlightSettings;

	fType->name = "AmigaGuide";

	stylename[0] = "Command";
	stylename[1] = "String argument";
	stylename[2] = "Other argument";
	stylename[3] = "Node and Endnode";
	stylename[4] = NULL;

	hlGUI = fType->createHighlightStylesGUI(stylename, defaultStyleCodes);
	fType->pluginData = (ULONG)hlGUI;

	fType->highlighterHook = hook = fType->createHighlighter(237+50, 0, hlGUI->inuseStylecodes);

	if (!hook)
		return;

	fType->createHighlightState(hook, 0, CHAR_NONWORD, 0); /* non word wildcard */

	fType->createHighlightState(hook, TEHL_STATE_SELFREPEATING, CHAR_ALWAYS, 0); /* always matches wildcard */

	/* all commands start with this one*/
	cmdstartstate = fType->createHighlightState(hook, 0, '@', 0);
	fType->linkHighlightState(hook, INITIALSTATE, cmdstartstate);

	/* Set up the attribute commands */
	setupAttribCommands(fType, hook, cmdstartstate);
	
	globalAttribState = setupGlobalAttribs(fType, hook);

	/* Set up the global commands */
	i=0;
	while (globalcommands[i])
	{
		ULONG s = fType->createHighlightKeyword(hook, globalcommands[i++], HL_KEYWORD, INITIALSTATE, FALSE);
		fType->linkHighlightState(hook, s, globalAttribState);
	}
	
	i=0;
	while (nodecommands[i])
	{
		ULONG s = fType->createHighlightKeyword(hook, nodecommands[i++], HL_NODEKEYWORD, INITIALSTATE, FALSE);
		fType->linkHighlightState(hook, s, globalAttribState);
	} 


	fType->linkHighlightState(hook, DEFAULTSTATE, cmdstartstate);
	fType->linkHighlightState(hook, DEFAULTSTATE, INITIALSTATE);
	

}
